import { SourcebitAspect } from './sourcebit.aspect';

export type { SourcebitMain } from './sourcebit.main.runtime';
export default SourcebitAspect;
export { SourcebitAspect };
