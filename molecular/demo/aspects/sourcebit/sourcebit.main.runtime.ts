import { MainRuntime } from '@teambit/cli';
import { SourcebitAspect } from './sourcebit.aspect';

export class SourcebitMain {
  static slots = [];
  static dependencies = [];
  static runtime = MainRuntime;
  static async provider() {
    return new SourcebitMain();
  }
}

SourcebitAspect.addRuntime(SourcebitMain);
