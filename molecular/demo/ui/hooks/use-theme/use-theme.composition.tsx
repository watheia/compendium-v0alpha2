import React from 'react';
import { useTheme } from './use-theme';

export const BasicuseTheme = () => {
  const { colorScheme } = useTheme();

  return (
    <>
      <h1>The color scheme is {colorScheme}</h1>
    </>
  );
};
