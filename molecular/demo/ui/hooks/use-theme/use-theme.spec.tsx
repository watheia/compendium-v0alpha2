import React from 'react';
import { renderHook, act } from '@testing-library/react-hooks';
import { useTheme } from './use-theme';

it('should increment counter', () => {
  const { result } = renderHook(() => useTheme());
  act(() => {
    result.current.setColorScheme('dark');
  });
  expect(result.current.colorScheme).toBe('dark');
});
