import { useState } from 'react';

type ColorScheme = 'light' | 'dark' | null;

export interface UseTheme {
  colorScheme: ColorScheme;
  setColorScheme: (colorScheme: ColorScheme) => void;
}
export function useTheme(): UseTheme {
  const [colorScheme, setColorScheme] = useState<ColorScheme>(null);
  return { colorScheme, setColorScheme };
}
