import { Aspect } from '@teambit/harmony';

export const MolecularEnvAspect = Aspect.create({
  id: 'watheia.molecular/envs/molecular-env',
});
