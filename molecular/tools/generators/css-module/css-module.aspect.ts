import { Aspect } from '@teambit/harmony';

export const CssModuleAspect = Aspect.create({
  id: 'watheia.molecular/tools/generators/css-module',
});
  