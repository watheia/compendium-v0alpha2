import { CssModuleAspect } from './css-module.aspect';

export type { CssModuleMain } from './css-module.main.runtime';
export default CssModuleAspect;
export { CssModuleAspect };
